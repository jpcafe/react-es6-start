# README #

 Description: React ES6 startup project.

 - Babel transpiling (ES6 and jsx)
 - Webpack dev server with hot reloading, module bundling and a proxy to an external express server (where you can develop a standard RESTful webservice etc)
 - Mocha/Chai/Enzyme stack for unit testing.
 - Features a separate RESTful API express server; available on the app URL, /api route.
 - Contains an example react component (testing included)
 

Commands: npm start, npm test

- API server runs on localhost:3000
- App server on localhost:3001 (env.port + 1)