// Dependencies
var express = require('express'),
	bodyParser = require('body-parser'),
	apiRouter = require('./routes/routes.js'),
	webpackDevServer = require('webpack-dev-server'),
	webpackConfig = require('./webpack.config.js'),
	webpack = require('webpack');

// Create Express server
var api = express();
api.use(bodyParser.json());

// set the static files location /public/img will be /img for users
//api.use(express.static(__dirname + '/dist')); 

// API routes
api.use('/api', apiRouter);

var port = process.env.PORT || 3000,
	devPort = port +1;

// Start API server
api.listen(port);
console.log('Api running on port ' + port);

// Create webpack dev server, proxy API requests to express server
new webpackDevServer(webpack(webpackConfig(devPort)), {
  proxy: {
    "/api" : "http://localhost:" + port
  },
  hot: true,
  historyApiFallback: true
}).listen(devPort);

console.log('App running on port ' + devPort);

// Expose app
exports = module.exports = api;  