import {expect} from 'chai';
import React from 'react';
import { shallow } from 'enzyme';
import Component1 from './../src/react/component1.jsx';

describe('Test < Component1 />', function () {

    it('should render container div with id component1', () => {
      let rootNode =  shallow(<Component1 />).find('#component1');

      expect(rootNode).to.have.length(1);
      expect(rootNode.type()).to.equal('div');
    });

    it('should render h3 title element with fixed text', () => {
      let componentNode =  shallow(<Component1 />).find('#title');

      expect(componentNode).to.have.length(1);
      expect(componentNode.text()).to.equal("My React Component");
      expect(componentNode.type()).to.equal("h3");
    });

    it('should render h4 subtitle element with text based on props', () => {
      let title = 'FakeTitle';
      let componentNode =  shallow(<Component1 title = {title} />).find('#subtitle');

      expect(componentNode).to.have.length(1);
      expect(componentNode.text()).to.equal(title);
      expect(componentNode.type()).to.equal("h4");
    });

    it('should render default subtitle element with default text, if title prop is not propagated', () => {
      let componentNode =  shallow(<Component1 />).find('#subtitle');

      expect(componentNode).to.have.length(1);
      expect(componentNode.text()).to.equal('Default Title');
    });

    it('should display an ul with li list based on prop topics, if present', () => {
      let topics = [ "Benfica", "Gaitan", "Ronaldo"];
      let componentNode =  shallow(<Component1 topics = {topics} />).find('#topics-list');

      expect(componentNode).to.have.length(1);
      expect(componentNode.type()).to.equal("ul");
      expect(componentNode.children()).to.have.length(3);
      expect(componentNode.childAt(0).text()).to.equal(topics[0]);
      expect(componentNode.children().every('li')).to.equal(true);
    });

    it('should display a div with class not-found and fixed text, when topics prop is not prosent', () => {
      let componentNode =  shallow(<Component1 />);
      let notFoundElement = componentNode.find('.not-found');

      expect(componentNode.find('#topics-list')).to.have.length(0);
      expect(notFoundElement).to.have.length(1);
      expect(notFoundElement.text()).to.equal("No topics found");
      expect(notFoundElement.type()).to.equal('div');
    });

});