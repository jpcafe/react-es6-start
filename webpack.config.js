    var path = require('path');
    var webpack = require('webpack');

    module.exports =  function (port) {
        return {
        entry: {app: [
            'webpack-dev-server/client?http://localhost:' + port + '/',
          'webpack/hot/dev-server',
            './src/main.jsx']},
        output: {
            path: path.resolve(__dirname, "/dist"),
            publicPath: "/dist/",
            filename: 'bundle.js'
        },
        module: {
            loaders: [
                { test: path.join(__dirname, 'src'),
                  loader: 'babel-loader' }
            ]
        },
         plugins: [  
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ]
    };

}

