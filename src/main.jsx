import Component1 from './react/component1.jsx';
import React from 'react';
import { render } from 'react-dom';

const topics = ['mocha', 'react', 'babel'];
const title = "ES6"

render(<Component1 topics = {topics} title = {title}/>, document.getElementById('app'));
