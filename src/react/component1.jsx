import React from 'react';

export default class Component1 extends React.Component {

    render () {
        const notFoundDiv = <div className = 'not-found' >No topics found</div>;
        return (
            <div id='component1'>
                <h3 id='title'>My React Component</h3>
                <h4 id='subtitle'>{this.props.title}</h4>
                {this.props.topics.length > 0 ? this.mapTopics() : notFoundDiv}
            </div>
        );
    }

    mapTopics () {
        let items = this.props.topics.map( (prop) => {
                return (<li key= {prop}>{prop}</li>);
            });
        return <ul id= 'topics-list'>{items}</ul>
    }
}

Component1.defaultProps = {topics: [], title: 'Default Title'}
